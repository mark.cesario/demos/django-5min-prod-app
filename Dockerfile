FROM python:3.9

COPY webapp .

RUN python -m pip install Django
RUN pip install psycopg2-binary

EXPOSE 8000

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
